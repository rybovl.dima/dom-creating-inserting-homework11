// 1. Створіть новий елемент <a>, задайте йому текст "Learn More" і атрибут href з посиланням на "#". Додайте цей елемент в footer після параграфу.

const newElement = document.createElement("a");
newElement.textContent = "Learn More";
newElement.setAttribute("href", "#");

const newFooter = document.createElement("footer");
const newParagraph = document.createElement("p");
document.body.append(newFooter);
newFooter.prepend(newParagraph);
newParagraph.after(newElement);


// 2. Створіть новий елемент <select>. Задайте йому ідентифікатор "rating", і додайте його в тег main перед секцією "Features".
// Створіть новий елемент <option> зі значенням "4" і текстом "4 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "3" і текстом "3 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "2" і текстом "2 Stars", і додайте його до списку вибору рейтингу.
// Створіть новий елемент <option> зі значенням "1" і текстом "1 Star", і додайте його до списку вибору рейтингу.

const newElementSelect = document.createElement("select");
newElementSelect.id = "rating";

const newMain = document.createElement("main");
const newFeatures = document.createElement("section");

document.body.prepend(newFeatures);
newFeatures.after(newMain);
newMain.prepend(newElementSelect);

const newRatings = [
    { value: "4", text: "4 Star" },
    { value: "3", text: "3 Star" },
    { value: "2", text: "2 Star" },
    { value: "1", text: "1 Star" }
];

newRatings.forEach(rating => {
    const newOption = document.createElement("option");
    newOption.value = rating.value;
    newOption.textContent = rating.text;
    newElementSelect.append(newOption);
});
